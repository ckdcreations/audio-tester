/******************************************************************************************************
 * Name: Clifford Dunn
 * Description: Audio Tests Script
 * File Name: leveltests.c
 * Language: C
 *****************************************************************************************************/

// Include Libraries
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>



#ifndef IP_ADDRESS
#define IP_ADDRESS          "10.4.0.65"
#endif

#ifndef PORT_NUM
#define PORT_NUM            48631
#endif

#ifndef PROTOCOL
#define PROTOCOL            0
#endif



/******************************************************************************************************
 * Function: SendMessage
 * Description: Sends a message to the remote server
 * Inputs: int for socket number, char string for message
 * Outputs: If error, will exit program
 *****************************************************************************************************/
void SendMessage(int socketFD, char msg[8])
{
    int errsrv;
    int buf = strlen(msg);
    int writeError = write(socketFD, msg, buf);

    // If there is an error writing the message to the socket, or if the message is incomplete, exit with error.
    if(writeError < 0)
    {
        errsrv = errno;
        fprintf(stderr, "Write error with code: %d\n", errsrv);
        exit(3);
    }
    else if(writeError >= 0 && writeError != buf)
    {
        perror("Write error: Incomplete message sent.\n");
        exit(3);
    }
}



/******************************************************************************************************
 * Function: ReceiveMessage 
 * Description: Receives a message to the remote server
 * Inputs: int for socket number, char string for message
 * Outputs: If error, will exit program
 *****************************************************************************************************/
void ReceiveMessage(int socketFD, char rcv[8])
{
        int errsrv, readError;
        readError = read(socketFD, rcv, 8);
        if(readError < 0)
        {
            errsrv = errno;
            fprintf(stderr, "Read error with code: %d\n", errsrv);
            exit(4);
        }
}




/******************************************************************************************************
 * Function: main 
 * Description: Main program function
 *****************************************************************************************************/
int main()
{
    int socketFD, msgLength, closerError, errsrv;
    int i, j, retVal, numErrors;
    ssize_t writeError, readError;
    struct sockaddr_in serverAddress;
    char *msg, fName[64], rcvMsg[8], msgBuffer[8];
    time_t t;
    struct tm tm;
    FILE *fp;


    // Setting up the server address
    memset((char *)&serverAddress, '\0', sizeof(serverAddress)); // Zero out the memory for the address
    serverAddress.sin_family = AF_INET; // Create a network-capable socket
    serverAddress.sin_port = htons(PORT_NUM); // Create the network port for the server
    serverAddress.sin_addr.s_addr = inet_addr(IP_ADDRESS); // Open a connection to the IP address of the server

       
    // Create the socket. AF_INET specifies normal internet based IP addresses.
    // SOCK_STREAM specifies a continuous stream requiring a TCP connection. 
    // SOCK_DGRAM would specify a UDP connection.
    if(PROTOCOL == 0)
    {
        if( (socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }
    else if(PROTOCOL == 1)
    {
        if( (socketFD = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }

    // Connect the socket to the server at the IP address
    if(connect(socketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Error connecting");
        exit(1);
    }


    // Create the time object in order to time stamp the file.
    t = time(NULL);
    tm = *localtime(&t);
    // Write the file name to a string. File name will inclde year, month, day, hour, minute, and second appended. 
    sprintf(fName, "results-%d-%d-%d %d:%d:%d.txt", tm.tm_year + 1900, tm.tm_mon +1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    // Open the file and prepare for writing
    fp = fopen(fName, "w");


    // Counter variable to determine errors
    numErrors=0;

    // In this for loop, iterate through every control number in the Composer file "Audio Tests" and query them.
    for(i=1; i < 296; i++)
    {


        // Create a string with the command 'gs %dr' where 'gs' is the Symetrix control command to Get Control and %d
        // is the control number in place.
        sprintf(msgBuffer, "gs %d\r", i);

        // Sleep in order to make sure command sends and receives on time
        sleep(0.25);
        // Send this message to the server.
        SendMessage(socketFD, msgBuffer);

        // Sleep in order to make sure command sends and receives on time
        sleep(0.25);
        // Read the response from the server. If there are errors, exit. Otherwise write the message to rcvMsg
        ReceiveMessage(socketFD, rcvMsg);

        // Parse the value that's in recvMsg. If it's an integer value that is non zero, pass through.
        // If the msg is a value of 0, there is an error with an audio module.
        // If the msg is "NAK", then the control number in question doesn't exist.
        // For errors, the control number will be written to a file with the specific error message.
        retVal = strtol(rcvMsg, &msg, 10);
        if(strncmp(msg, "NAK", 3) == 0)
        {
            fprintf(fp, "Control number %d does not exist.\n", i);
            numErrors++;
        }
        else if(retVal == 0)
        {
            fprintf(fp, "Error at control number %d\n", i);
            numErrors++;
        }

        // When control number 69 is called, the next number is part of an LCR panner.
        // As a result, this panner must be set for the left channel in order to monitor
        if(i == 69)
        {
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
            sprintf(msgBuffer, "LP 1\r");
            SendMessage(socketFD, msgBuffer);
            ReceiveMessage(socketFD, rcvMsg);
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
        }

        // When control number 81 is called, the next number is part of an LCR panner.
        // As a result, this panner must be set for the center channel in order to monitor
        if(i == 81)
        {
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
            sprintf(msgBuffer, "LP 2\r");
            SendMessage(socketFD, msgBuffer);
            ReceiveMessage(socketFD, rcvMsg);
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
        }

        // When control number 93 is called, the next number is part of an LCR panner.
        // As a result, this panner must be set for the right channel in order to monitor
        if(i == 93)
        {
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
            sprintf(msgBuffer, "LP 3\r");
            SendMessage(socketFD, msgBuffer);
            ReceiveMessage(socketFD, rcvMsg);
            // Sleep in order to make sure command sends and receives on time
            sleep(1);
        }
    } 

    // After the loop is finished, close the socket.
    if(close(socketFD) < 0)
    {
        errsrv = errno;
        fprintf(stderr, "Error closing socket with code: %d.\n", errsrv);
        exit(5);
    }

    // Close the file being written
    fclose(fp);

    // Report the errors back to the user.
    if(numErrors == 0)
    {
        fprintf(stdout, "No errors found!\n");
        remove(fName);
    }
    else
        fprintf(stdout, "%d errors found. Please consult %s\n", numErrors, fName);


    return 0;

}